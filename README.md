# Nginx durci

Configuration Nginx minimale, durcie avec différentes recommandations de sécurité (CIS, Nginx, ANSSI).

## Image Docker
Une image docker est disponible : 

`
docker pull registry.gitlab.com/securefab/nginx
`

Basée sur l'image nginx [nginxinc/nginx-unprivileged](https://hub.docker.com/r/nginxinc/nginx-unprivileged).
L'image est testée avec Nginx 1.19.3, compilée avec openssl1.1.1 (TLS1.3) et le module brotli.

## Déploiement 

 * Le contenu du dossier nginx doit être copier dans /etc/nginx

 * Les variables entourées de ** sont à remplacer.

 * Les certificats et dhparam (*.pem) sont à générer.

 * Modifier la configuration pour fonctionner avec votre projet

### Génération du certificat

## Auto signé (pour test uniquement)
```bash
openssl req -subj '/CN=172.17.0.1/O=company/C=FR' -x509 -newkey rsa:4096 -days 60 -nodes > site.pem
```

### Génération du dhparam
```bash
openssl dhparam -dsaparam -out dhparam.pem 4096
```
Ou alors :
```bash
 docker run openssl dhparam -dsaparam 4096
```
### Examples de configurations utiles 

#### Static resources (server)
```c
location /static/ {
  alias /var/www/static/;
  expires 30d;
}
```
